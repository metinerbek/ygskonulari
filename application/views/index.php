<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="tr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>YGS Konu Dağılımları</title>




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55224281-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="<?php echo base_url();?>js/jquery-1.9.1.min.js"></script>

<script src="<?php echo base_url();?>js/jquery.widget.min.js"></script>


<script src="<?php echo base_url();?>js/bootstrap.js"></script>
<script src="<?php echo base_url();?>js/metro.min.js"></script>

<link href="<?php echo base_url();?>css/metro-bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url()."css/metro-responsive.min.css";?>" rel="stylesheet">
<script type="text/javascript">
function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}	

var emailvarmi=0;
function emailuniquekontrol(){


	 $.ajax({
		 url:"<?php echo site_url()."/register/emailvarmikayit/";?>",
		 type:"POST",
		 data:$("#kayitformu").serialize(),
		 async:false,
		 success:function(cikti){
	
			 if(cikti!=0){
			 emailvarmi=1;
			 }else{
			emailvarmi=0
			 
			 }
		
	
		}
		
		 
		});//ajaxin

}


function kayitol(){
if($("#sifre").val()==""){
alert("Bir Şifre Giriniz.");
return false;
}
if($("#adsoyad").val()==""){
alert("Bir Şifre Giriniz.");
return false;
}
if($("#email").val()==""){
alert("Bir Email Giriniz.");
return false;
}
if(!IsEmail($("#email").val())){
alert("Email Adresinizi Kontrol Ediniz.");
return false;
}

emailuniquekontrol();


if(emailvarmi==1){
alert("Bu email ile kayıt olmuş bir üye var.");
emailvarmi=0;
return false;
}


return true;
}

function giris(){
if($("#sifreG").val()==""){
alert("Bir Şifre Giriniz.");
return false;
}

if($("#emailG").val()==""){
alert("Bir Email Giriniz.");
return false;
}
if(!IsEmail($("#emailG").val())){
alert("Email Adresinizi Kontrol Ediniz.");
return false;
}
return true;
}


</script>


<meta name="description" content="Ygs konuları" />
<meta name="robots" content="index, follow">
<meta name="keywords" content="ygs konuları,ygs konu dağılımları,ygs 2015 konu dağılımları,ygs tüm konular,ygsde konular neler,ygs konu takibi">




</head>
<body class="metro">

<?php require("menusecimi.php");?>
<br>

<div style="float:left;width:75%">
<center>
<br><h2>Son Yıllardaki YGS Konu Dağılımları</h2><br>
</center>
<?php require("multiliste.php");?>
</div>

<div id="sen" style="float:right;width:25%;text-align:right;padding-right:3%">
<br><br>
<br><br>

<form id="girisform" method="post" action="<?php echo site_url()."/login/giris"?>" onsubmit="return giris()">
<table  class="table" style="width:100%;border:1px solid #000">
<tr class="success"><td style="text-align:left;" colspan="2">Giriş Yapın.</td></tr>
<tr>
<td>Email :</td>
<td style="text-align:left"><input type="text" name="emailG" id="emailG"></td>
</tr>

<tr>
<td>Şifre :</td>
<td style="text-align:left"><input type="password" name="sifreG" id="sifreG"></td>
</tr>
<tr>
<td colspan="2" style="text-align:center"><input type="submit" value="Giriş"></td>

</tr>
</table>
</form>

<br><br>
<!--<a href="javascript:void(0)" onclick="uyeol()" class="button fg-white bg-green">Üye ol</a>-->
<form id="kayitformu" method="post" action="<?php echo site_url()."/register/kayit"?>" onsubmit="return kayitol()">
<table  class="table" style="width:100%;border:1px solid #000">
<tr class="info"><td style="text-align:left;" colspan="2">Konularınızı takip edebilmek için üye olabilirsiniz.</td></tr>
<tr>
<td>Email :</td>
<td style="text-align:left"><input type="text" name="email" id="email"></td>
</tr>
<tr>
<td>İsim :</td>
<td style="text-align:left"><input type="text" name="adsoyad" id="adsoyad"></td>
</tr>
<tr>
<td>Şifre :</td>
<td style="text-align:left"><input type="password" name="sifre" id="sifre"></td>
</tr>
<tr>
<td colspan="2" style="text-align:center"><input type="submit" value="kaydol"></td>

</tr>
</table>
</form>
</div>



</body>
</html>