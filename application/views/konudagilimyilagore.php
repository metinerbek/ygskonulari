<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $yili; ?> Yılı YGS Konu Dağılımları</title>

<meta name="description" content="<?php echo $yili; ?> yılı soru dağılımları" />
<meta name="robots" content="index, follow">
<meta name="keywords" content="<?php echo $yili; ?> yılı soru dağılımları,ygs konuları,ygs konu dağılımları,ygs 2015 konu dağılımları,ygs tüm konular,ygsde konular neler,ygs konu takibi" />	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55224281-1', 'auto');
  ga('send', 'pageview');

</script>	
<script src="<?php echo base_url();?>js/jquery-1.9.1.min.js"></script>

<script src="<?php echo base_url();?>js/jquery.widget.min.js"></script>


<script src="<?php echo base_url();?>js/bootstrap.js"></script>
<script src="<?php echo base_url();?>js/metro.min.js"></script>

<link href="<?php echo base_url();?>css/metro-bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url()."css/metro-responsive.min.css";?>" rel="stylesheet">

</head>
<body class="metro">

<?php require("menusecimi.php");?>
<center>
<br><h2><?php echo $yili;?> Yılı YGS Konu Dağılımları</h2><br>
</center>
<?php require("multiliste.php");?>
</body>
</html>