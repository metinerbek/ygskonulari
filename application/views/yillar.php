<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Language" content="tr" />

<meta name="robots" content="noindex,nofollow">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="<?php echo base_url();?>js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url();?>js/metro.min.js"></script>

<link href="<?php echo base_url();?>css/metro-bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet">


<title>Admin Panel</title>
<style type="text/css">

</style>


<script type="text/javascript">

function anasayfadadegisim(id){
var deger=0;
if($("#dersanasayfadami"+id).is(":checked")){
deger=1;
}else{
deger=0;
}
//alert(deger)

$.ajax({
		 url:"<?php echo site_url().'/admin/anasayfadurumu/';?>"+id+"/"+deger,
		 type:"POST",		
success:function(cikti){

if(cikti!=0){

	alert("Tamamlandı.");
}else{

alert("Bir sorun oluştu");

}
}




});

}

function yilekleme(){

$.ajax({
		 url:"<?php echo site_url().'/admin/yilekleme/';?>",
		 type:"POST",
		 data:$("#keform").serialize(),
		
success:function(cikti){

if(cikti!=0){
$("#yillartablo").append(cikti)
	//alert(cikti);
	$("#yiladi").val("");
	alert("Yıl Eklendi");
}else{

alert("Bir sorun oluştu");

}
}




});
return false;

}


function sil(id){

$.ajax({
		 url:"<?php echo site_url().'/admin/yilsil/';?>"+id,
		 type:"POST",
		
success:function(cikti){

if(cikti!=0){
$("#yili"+id).remove()
	//alert(cikti);
	alert("Yıl Silindi");
}else{

alert("Bir sorun oluştu");

}
}




});
return false;

}




</script>
</head>

<body class="metro">
<?php
require("adminmenu.php");
?>
<br /><br /><br />
<form id="keform" onsubmit="return yilekleme();">
<table align="center" style="">


<tr>
<td>Yıl :</td>
<td><input type="text" name="yiladi" id="yiladi"></td>
</tr>


<tr>
<td colspan="2" style="text-align:center"><input type="submit" value="Ekle"></td>

</tr>




</table>
</form>
<br><br>

<table style="width:25%" align="center" id="yillartablo" class="table bordered hovered">
<tr class="info">
<td>Yıl</td>
<td>Yıl Anasayfada Mı ?</td>
<td>Sil</td>
</tr>

<?php
foreach($yillar->result() as $yil){
?>
<tr id="yili<?php echo $yil->yilId;?>" style="text-align:center;">
<td><h4><?php echo $yil->yil;?></h4></td>


<td>
<div class="input-control switch">
    <label>
        <input type="checkbox" id="dersanasayfadami<?php echo $yil->yilId;?>" value="1" onchange="anasayfadadegisim(<?php echo $yil->yilId;?>);" <?php if($yil->nerede==1){echo "checked";}?>>
        <span class="check"></span>
    </label>
</div>

</td>
<td><input type="button" value="Sil" onclick="sil(<?php echo $yil->yilId;?>)"></td>

</tr>


 <?php
 
 }
 ?>
</table>




</body>

</html>